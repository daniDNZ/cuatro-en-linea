# frozen_string_literal: true

def colorize(code)
  "\e[#{code}mO\e[0m"
end

def red
  colorize(31)
end

def green
  colorize(32)
end
