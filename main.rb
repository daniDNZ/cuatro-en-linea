# frozen_string_literal: true

require './classes/player'
require './classes/board'
require './classes/game'
require './utils/colorize'

begin
  system 'clear'
  puts ''
  puts 'CUATRO EN LÍNEA'
  puts ''
  puts 'Instrucciones: '
  puts 'Para colocar tu ficha, escribe el número de la columna: '
  puts ''
  puts '1 | 2 | 3 | 4 | 5 | 6 | 7'
  puts ''

  puts 'Nick player 1:'
  @player_one = Player.new(gets.chomp, red)

  puts 'Nick player 2:'
  @player_two = Player.new(gets.chomp, green)

  @board = Board.new(6, 7)

  @game = Game.new(@player_one, @player_two, @board)
  @game.start

  system 'clear'
  @game.board.display_board
  puts ''
  if @game.winner.length.positive?
    puts "The winner is #{@game.winner.upcase}"
  else
    puts 'Esto es un empate'
  end
end
