# frozen_string_literal: true

# Game board
class Board
  def initialize(rows, columns)
    @rows = rows
    @columns = columns
    @board = Array.new(@rows) { Array.new(@columns, '.') }
    @last_coord = {}
  end

  attr_reader :rows, :columns, :last_coord

  def get_cell(row, column)
    @board[row][column]
  end

  def display_board
    puts '| 1 | 2 | 3 | 4 | 5 | 6 | 7 |'
    row = 0
    while row <= 5
      print_cells(row)
      puts ''
      row += 1
    end
    puts ''
  end

  def throw_piece(column, color)
    row = 5
    while row >= 0
      if get_cell(row, column) == '.'
        set_cell(row, column, color)
        @last_coord = { x: column, y: row }
        break
      end
      row -= 1
    end
  end

  private

  def set_cell(row, column, data)
    @board[row][column] = data
  end

  def print_cells(row)
    column = 0
    while column <= 6
      print "| #{@board[row][column]} "
      column += 1
    end
    print '|'
  end
end
