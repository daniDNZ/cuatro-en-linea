# frozen_string_literal: true

# Clase game
class Game
  def initialize(player_one, player_two, board)
    @current_turn = 0
    @current_player = nil
    @player_one = player_one
    @player_two = player_two
    @winner = ''
    @board = board
  end

  attr_reader :board, :winner

  def start
    while @current_turn < 21
      @current_player = @player_one
      turn
      break if @winner.length.positive?

      @current_player = @player_two
      turn
      break if @winner.length.positive?

      @current_turn += 1
    end
  end

  private

  def check_player_input
    regex = /^[1-7]$/
    column = '0'
    until column.match(regex)
      puts "[#{@current_player.name.upcase}] Enter Column [1-7]: "
      input = gets.chomp
      column = input if input.match(regex) && @board.get_cell(0, input.to_i - 1) == '.'
    end
    column.to_i - 1
  end

  # Check horizontal line
  def check_h_line
    column = 0
    row = @board.last_coord[:y]
    piece_counter = 0
    until column == @board.columns
      @board.get_cell(row, column) == @current_player.color ? piece_counter += 1 : piece_counter = 0
      break if piece_counter == 4

      column += 1
    end
    piece_counter >= 4
  end

  # Check vertical line
  def check_v_line
    row = 0
    column = @board.last_coord[:x]
    piece_counter = 0
    until row == @board.rows
      @board.get_cell(row, column) == @current_player.color ? piece_counter += 1 : piece_counter = 0
      break if piece_counter == 4

      row += 1
    end
    piece_counter >= 4
  end

  # Get left first coord
  def desc_d_start_coord
    tmp_coords = { y: @board.last_coord[:y], x: @board.last_coord[:x] }
    until tmp_coords[:y].zero? || tmp_coords[:x].zero?
      tmp_coords[:y] -= 1
      tmp_coords[:x] -= 1
    end
    tmp_coords
  end

  # Check descendant diagonal
  def check_desc_d_line
    tmp_coords = desc_d_start_coord
    piece_counter = 0
    until tmp_coords[:y] == @board.rows || tmp_coords[:x] == @board.columns
      @board.get_cell(tmp_coords[:y], tmp_coords[:x]) == @current_player.color ? piece_counter += 1 : piece_counter = 0
      break if piece_counter == 4

      tmp_coords[:y] += 1
      tmp_coords[:x] += 1
    end
    piece_counter >= 4
  end

  # Get left first coord
  def asc_d_start_coord
    tmp_coords = { y: @board.last_coord[:y], x: @board.last_coord[:x] }
    until tmp_coords[:y] == @board.rows - 1 || tmp_coords[:x].zero?
      tmp_coords[:y] += 1
      tmp_coords[:x] -= 1
    end
    tmp_coords
  end

  # Check ascendant diagonal
  def check_asc_d_line
    tmp_coords = asc_d_start_coord
    piece_counter = 0
    until tmp_coords[:y].zero? || tmp_coords[:x] == @board.columns
      @board.get_cell(tmp_coords[:y], tmp_coords[:x]) == @current_player.color ? piece_counter += 1 : piece_counter = 0
      break if piece_counter == 4

      tmp_coords[:y] -= 1
      tmp_coords[:x] += 1
    end

    piece_counter >= 4
  end

  def check_winner
    check_h_line || check_v_line || check_desc_d_line || check_asc_d_line
  end

  def turn
    system 'clear'
    @board.display_board
    column = check_player_input
    @board.throw_piece(column, @current_player.color)
    @winner = @current_player.name if @winner.length.zero? && check_winner
  end
end
